package com.example.mikitashah.braintrainer.models

data class SumNumbers(val number1: Int, val number2: Int)