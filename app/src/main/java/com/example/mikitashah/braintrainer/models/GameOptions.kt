package com.example.mikitashah.braintrainer.models

data class GameOptions(val options: ArrayList<Int>, val correctAnswerIndex: Int)