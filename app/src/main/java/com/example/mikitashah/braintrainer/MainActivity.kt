package com.example.mikitashah.braintrainer

import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.os.CountDownTimer
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import com.example.mikitashah.braintrainer.models.SumNumbers
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    private val TAG: String = this.javaClass.simpleName

    private lateinit var countDownTimer: CountDownTimer
    private lateinit var gameViewModel: GameViewModel

    private var isTimeUp = false

    private val countDownTime = 30 * 1000L
    private val countDownBuffer = 100L
    private val countDownInterval = 1000L

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        initial_view.visibility = VISIBLE
        game_stats.visibility = GONE
        options_grid.visibility = GONE
        gameViewModel = ViewModelProviders.of(this).get(GameViewModel::class.java)

    }

    fun startGame(view: View) {
        initial_view.visibility = GONE
        game_stats.visibility = VISIBLE
        options_grid.visibility = VISIBLE

        showQuestion()
        startTimer()
    }

    private fun showQuestion() {
        val sumNumbers = gameViewModel.getSumNumbers()
        game_question.text = resources.getString(R.string.game_question,
                sumNumbers.number1, sumNumbers.number2)

        val allOptions: List<Int> = gameViewModel.getGameOptions().options

        Log.d(TAG, "allOptions: $allOptions")

        option0.text = allOptions[0].toString()
        option1.text = allOptions[1].toString()
        option2.text = allOptions[2].toString()
        option3.text = allOptions[3].toString()
    }

    private fun startTimer() {
        countDownTimer = object : CountDownTimer(countDownTime + countDownBuffer, countDownInterval) {
            override fun onTick(millisUntilFinished: Long) {
                countdown_timer.text = resources.getString(R.string.countdown_time, (millisUntilFinished / 1000))
            }

            override fun onFinish() {
                countdown_timer.text = resources.getString(R.string.countdown_time, 0)
                isTimeUp = true
                Log.d(TAG, "Time up!")
            }
        }
        isTimeUp = false
        countDownTimer.start()
    }

    fun onOptionClick(view: View) {
        if (gameViewModel.isAnswerCorrect(view.tag as String)) {
            result.text = resources.getString(R.string.correct_answer)
        } else {
            result.text = resources.getString(R.string.wrong_answer)
        }
        result.visibility = VISIBLE
        player_score.text = resources.getString(R.string.game_score,
                gameViewModel.getGameScore().totalCorrectAnswers, gameViewModel.getGameScore().totalQuestions)
        if (isTimeUp) {
            option0.isEnabled = false
            option1.isEnabled = false
            option2.isEnabled = false
            option3.isEnabled = false
            showFinalScore()
            play_again.visibility = VISIBLE
        } else {
            showQuestion()
        }
    }

    fun onRestartGame (view: View){
        gameViewModel.restartGame()
        player_score.text = resources.getString(R.string.game_score,
                gameViewModel.getGameScore().totalCorrectAnswers, gameViewModel.getGameScore().totalQuestions)

        option0.isEnabled = true
        option1.isEnabled = true
        option2.isEnabled = true
        option3.isEnabled = true
        result.visibility = GONE

        play_again.visibility = GONE

        showQuestion()
        startTimer()
    }

    private fun showFinalScore() {
        result.text = resources.getString(R.string.final_score,
                gameViewModel.getGameScore().totalCorrectAnswers, gameViewModel.getGameScore().totalQuestions)
    }
}
