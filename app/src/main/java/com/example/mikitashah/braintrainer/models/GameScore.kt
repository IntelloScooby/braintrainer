package com.example.mikitashah.braintrainer.models

class GameScore {
    var totalCorrectAnswers: Int
    var totalQuestions: Int

    init {
        totalCorrectAnswers = 0
        totalQuestions = 0
    }

    fun clear() {
        totalCorrectAnswers = 0
        totalQuestions = 0
    }
}