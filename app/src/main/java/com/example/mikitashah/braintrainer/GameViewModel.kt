package com.example.mikitashah.braintrainer

import android.arch.lifecycle.ViewModel
import android.util.Log
import com.example.mikitashah.braintrainer.models.GameOptions
import com.example.mikitashah.braintrainer.models.GameScore
import com.example.mikitashah.braintrainer.models.SumNumbers
import java.util.*

class GameViewModel : ViewModel() {

    private val TAG = this.javaClass.simpleName

    private var sumNumbers: SumNumbers? = null
    private var gameOptions: GameOptions? = null
    private val gameScore = GameScore()
    private val random = Random()
    private val MAX_NUMBER: Int = 21
    private val TOTAL_NUMBER_OF_OPTIONS: Int = 4

    fun getSumNumbers(): SumNumbers {
        if (sumNumbers == null) {
            sumNumbers = SumNumbers(random.nextInt(MAX_NUMBER), random.nextInt(MAX_NUMBER))
        }
        return sumNumbers as SumNumbers
    }

    fun isAnswerCorrect(selectedAnswerLocation: String): Boolean {
        sumNumbers = null
        gameScore.totalQuestions += 1
        if (selectedAnswerLocation == gameOptions?.correctAnswerIndex.toString()) {
            gameScore.totalCorrectAnswers += 1
            gameOptions = null
            return true
        }
        gameOptions = null
        return false
    }

    fun getGameOptions(): GameOptions {
        if (gameOptions == null) {
            val correctOptionLocation = random.nextInt(TOTAL_NUMBER_OF_OPTIONS)
            val options = ArrayList<Int>(TOTAL_NUMBER_OF_OPTIONS)
            val sum = getSumNumbers().number1 + getSumNumbers().number2
            for (i in 0 until TOTAL_NUMBER_OF_OPTIONS) {
                if (i == correctOptionLocation) {
                    options.add(i, sum)
                } else {
                    var randomOption = random.nextInt(MAX_NUMBER + MAX_NUMBER)
                    while (options.contains(randomOption) || randomOption == sum) {
                        randomOption = random.nextInt(MAX_NUMBER + MAX_NUMBER)
                    }
                    options.add(i, randomOption)
                }
            }
            gameOptions = GameOptions(options, correctOptionLocation)
        }
        return gameOptions as GameOptions
    }

    fun getGameScore(): GameScore {
        return gameScore
    }

    fun restartGame(){
        gameScore.clear()
        gameOptions = null
        sumNumbers = null
    }

}